%include "lib.inc"

section .text
global find_word
find_word:
	xor rax,rax
	.check_key:
		add rsi,8 ;добавляем смещение к значению ключа
		push rdi
		push rsi
		call string_equals ; сравниваем строки (возвращает 1, если равны, и 0, если не равны, в rax)
		pop rsi
		pop rdi
		cmp rax,0 ; если строки равны
		jne .found ; то переход в .found
		sub rsi,8 ; иначе переходим к следующему значнию в словаре
		mov rsi,qword[rsi]
		cmp rsi,0 ; если следующего элемента нет
		je .no_such_key ; то переход в .no_such_key
		jmp .check_key ; проверка следующего элемента
	
	.found: 
		sub rsi,8 ; смещаем адрес до начала вхождения
		mov rax,rsi ; ключ найден, возвращаем адрес начала вхождения в словарь
		ret
	
	.no_such_key: 
		xor rax,rax ; ключ не найден, возвращаем 0
		ret
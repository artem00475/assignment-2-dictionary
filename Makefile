ASM=nasm
ASMFLAGS=-f elf64
LD=ld
FILES=lib.o main.o dict.o

prog: $(FILES)
	$(LD) -o $@ $+
	
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
	
%.asm: %.inc
	touch $@
	
main.asm: words.inc colon.inc
	touch $@
	
.PHONY: clean
clean:
	$(RM) *.o
	$(RM) prog

	

	

	
	
%include "lib.inc"
%include "dict.inc"
%define buffer_size 256
section .rodata
%include "words.inc"
no_such_key_str: db 'Такой ключ не найден в словаре',0xA,0
empty_dict_str: db 'В словаре нет  значений',0xA,0
buffer_limit_str: db 'Превышен допустимый размер буфера',0xA,0
enter_key: db 'Введите ключ: ',0
value_by_key: db 'Значение по ключу: ',0

section .bss
buffer: resb buffer_size ; выделяем буфер для считываемой строки

section .text
global _start
_start:
	mov rsi,first_element ; адрес первого элемента словаря
	cmp rsi,0 ; если его адрес - 0
	je .empty_dict ; значит словарь пустой
	push rsi
	.read_key:
		mov rdi,enter_key
		call print_string
		mov rdi,buffer ; задаем адрес начала буфера
		mov rsi,buffer_size ; задаем размер буфера
		call read_string ; считываем строку в буфер
		cmp rax,0 ; проверяем поместилась ли она в буфер
		je .buffer_limit ; если нет, то ошибка
		mov rdi,rax ; помещаем в rdi адрес начала буфера
	.find_key:
		pop rsi
		call find_word ; ищем ключ в словаре (не найден - 0 в rax)
		cmp rax,0
		je .no_such_key ; если слово не найдено, то ошибка
	.print_value: ; иначе выводим его в stdin
		push rax
		mov rdi,value_by_key
		call print_string
		pop rax ; в rax адрес начала вхождения в словарь
		add rax,8 ; смещение к адресу начала ключа
		push rax
		mov rdi,rax
		call string_length ; вычисляем смещение к адресу начала значения
		mov rcx,rax
		pop rax
		add rax,rcx ; смещаем на полученное значение
		inc rax ; смещаем на 1 (0-термиеированная строка)
		mov rdi, rax ; выводим значение
		call print_string
		call exit
	;ошибка: в словаре нет такого ключа
	.no_such_key: 
		mov rdi,no_such_key_str
		jmp .print_error_and_exit
	
	;ошибка: словарь пуст
	.empty_dict: 
		mov rdi,empty_dict_str
		jmp .print_error_and_exit
	
	;ошибка: превышен размер буфера
	.buffer_limit:
		mov rdi,buffer_limit_str
		jmp .print_error_and_exit

	.print_error_and_exit:
		call print_error
		call exit
